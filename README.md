# ReNom 

## Author

[GitLab](https://gitlab.com/ogeez)


## ReNom 

Detects if a file cannot be viewed under normal circumstances due to its filename makeup.
Applies a fix to files delimited by an underscore in the current directory.

## Fixes the following: 

- jpg | JPG 
- jpeg | JPEG
- png | PNG
- gif | GIF
- and many more when fine tuned to your liking

## Installation

### Method #1 — Clone via HTTPS

- Escalate to root `sudo bash`
- Run `yum -y install git`
- `cd` into target destination
- Run `git clone https://gitlab.com/bash-scriptz/utilities/renom.git`

### Method #2 — Download via Wget

- Run `wget https://gitlab.com/bash-scriptz/utilities/-/archive/master/renom-master.tar`
- `cd` into target destination
- Extract using `tar xf renom.tar` 

## Syntax  

View available commands: `./manage`<br>
Fix jpg files: `./manage jpg`<br>
Fix jpeg files: `./manage jpeg`<br>
Fix png files: `./manage png`<br>
Fix gif files: `./manage gif`

